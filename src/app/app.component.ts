import {Component, OnInit} from '@angular/core';
import {BeelineService} from './beeline.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  beelinePage: any;

  beeUrl = 'http://sms.beeline.kg';

  constructor(private beelineService: BeelineService) {
  }

  ngOnInit() {
    this.beelineService.getBeelinePage().subscribe((data) => {
      this.beelinePage = data;
    });
  }

}
