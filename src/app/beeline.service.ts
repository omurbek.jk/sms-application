import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BeelineService {

  // beelineUrl = 'http://sms.beeline.kg';
  beelineUrl = 'http://sms.beeline.kg/kcaptcha/index.php';

  constructor(private http: HttpClient) {
  }

  getBeelinePage() {
    return this.http.get(this.beelineUrl, {responseType: 'text'});
    // return this.http.get('https://jsonplaceholder.typicode.com/posts/1');
  }
}
