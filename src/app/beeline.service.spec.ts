import { TestBed, inject } from '@angular/core/testing';

import { BeelineService } from './beeline.service';

describe('BeelineService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BeelineService]
    });
  });

  it('should be created', inject([BeelineService], (service: BeelineService) => {
    expect(service).toBeTruthy();
  }));
});
